﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void SimpleShoot::Start()
extern void SimpleShoot_Start_m54F4A55B09D39783AA13EC8C78A69811D3ACC861 (void);
// 0x00000002 System.Void SimpleShoot::Update()
extern void SimpleShoot_Update_m9C86B96646484025B6FAA2BF986ED0370E4299BA (void);
// 0x00000003 System.Void SimpleShoot::Shoot()
extern void SimpleShoot_Shoot_m442002EA156056F2E9CA22132606BC9B8408A6CE (void);
// 0x00000004 System.Void SimpleShoot::CasingRelease()
extern void SimpleShoot_CasingRelease_m220FA905533E5D3C7628C0FF5E3C2828FDBD4A55 (void);
// 0x00000005 System.Void SimpleShoot::.ctor()
extern void SimpleShoot__ctor_mB5F461A08C2C7E7956B0AADEC89F252972313AA8 (void);
// 0x00000006 System.Void AI::Start()
extern void AI_Start_m84B3EA629AEA323FDE537C483BE498BA1E047D61 (void);
// 0x00000007 System.Void AI::Update()
extern void AI_Update_m44C17A8324CC9BF2827D98A82A1B26A8236E1CD4 (void);
// 0x00000008 System.Void AI::EnemyPath()
extern void AI_EnemyPath_mAB1712BD9A2CEFBF342933A95E28C136036CDFC9 (void);
// 0x00000009 System.Void AI::FollowPlayer()
extern void AI_FollowPlayer_m5DA2804644A20D44E2A589BAAFB8E0E1354F903E (void);
// 0x0000000A System.Void AI::.ctor()
extern void AI__ctor_m8D5C288A0A62DF5D6327580B33CF073B77D18026 (void);
// 0x0000000B System.Void CameraLook::Start()
extern void CameraLook_Start_m805BDFF07875BAB3146A92FABD13FD6AA1F732CD (void);
// 0x0000000C System.Void CameraLook::Update()
extern void CameraLook_Update_m8F7AE402EBD962C01D9CC6A3392316380BB70405 (void);
// 0x0000000D System.Void CameraLook::.ctor()
extern void CameraLook__ctor_mB43C0BBBA42F69696CD1B5AB9DC3F838FF4471AD (void);
// 0x0000000E System.Void Menu::Start()
extern void Menu_Start_m50A1B94A03D9E9BE1E2B69E931A1E02943BB2F36 (void);
// 0x0000000F System.Void Menu::Update()
extern void Menu_Update_mDF23A59FAEDA72A8B115C4C3CF2ACCAFC6B415AD (void);
// 0x00000010 System.Void Menu::EscenaJuego()
extern void Menu_EscenaJuego_m05F7043C4A9ADA7D61DDC0C5D20490C70457406A (void);
// 0x00000011 System.Void Menu::Salir()
extern void Menu_Salir_m25A4A2994265CFD84F621FD013BBDDAC2A51E2BA (void);
// 0x00000012 System.Void Menu::.ctor()
extern void Menu__ctor_m7EE9043A2E9DD126B6F69DD8AE3BC9CBDF6E2134 (void);
// 0x00000013 System.Void PlayerInteractions::OnTriggerEnter(UnityEngine.Collider)
extern void PlayerInteractions_OnTriggerEnter_m4F14223B353C818AF0F0C563C39BDBA62D867118 (void);
// 0x00000014 System.Void PlayerInteractions::.ctor()
extern void PlayerInteractions__ctor_m156B0757CF638271E3CF68D59F33B17E51802C6A (void);
// 0x00000015 System.Void PlayerMovement::Update()
extern void PlayerMovement_Update_mC3491BD6CDFF1FA543B16969144C939B2298052F (void);
// 0x00000016 System.Void PlayerMovement::.ctor()
extern void PlayerMovement__ctor_mBF9F632DD9929DD6FF092A968649A4406BFE397F (void);
// 0x00000017 System.Void Bullet::OnCollisionEnter(UnityEngine.Collision)
extern void Bullet_OnCollisionEnter_m828563825F2051114D4AEB3F356E3B12C0A9B424 (void);
// 0x00000018 System.Void Bullet::.ctor()
extern void Bullet__ctor_mC7D931FE508342F413FBA79525A4819D4114B3EC (void);
// 0x00000019 System.Void Gun::Update()
extern void Gun_Update_m71CB7FCF54EBD12F5A1452B9E5FC978F3636B04B (void);
// 0x0000001A System.Void Gun::.ctor()
extern void Gun__ctor_mAB78FE575637ED35491FD9880E4B35EB48689718 (void);
// 0x0000001B System.Void WeaponSway::Start()
extern void WeaponSway_Start_m069073D7D5D73F7958FD846EC69C59B3460514CA (void);
// 0x0000001C System.Void WeaponSway::Update()
extern void WeaponSway_Update_m1C67B5A19A29DA854753B4C189A9247836E93B7D (void);
// 0x0000001D System.Void WeaponSway::Sway()
extern void WeaponSway_Sway_m791BBE224F45B911464D99B824CDD7B74F6B5EA8 (void);
// 0x0000001E System.Void WeaponSway::.ctor()
extern void WeaponSway__ctor_m66F552B7D71D356878C3B25FE008DEA34A684E19 (void);
// 0x0000001F System.Void AmmoBox::.ctor()
extern void AmmoBox__ctor_m51D58B4CCFE518EA670AEF8F2F56189BACB8F5B6 (void);
// 0x00000020 GameManager GameManager::get_Instance()
extern void GameManager_get_Instance_m758C0D98BBDEF0C2DCA163A5D60B22DBB5B07232 (void);
// 0x00000021 System.Void GameManager::set_Instance(GameManager)
extern void GameManager_set_Instance_mAEF75ABE6135BDD61B9A3AED1606C0449787C5C7 (void);
// 0x00000022 System.Void GameManager::Awake()
extern void GameManager_Awake_m22F42B2A82708B10F652CAD8F2E0A4767110FF30 (void);
// 0x00000023 System.Void GameManager::Update()
extern void GameManager_Update_mC9303BA7C3117BD861F49F8E36151CC52117E6C1 (void);
// 0x00000024 System.Void GameManager::.ctor()
extern void GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D (void);
// 0x00000025 System.Void Readme::.ctor()
extern void Readme__ctor_mF465410C5B2E598F2685E82CFCE1F42186AFF448 (void);
// 0x00000026 System.Void Readme/Section::.ctor()
extern void Section__ctor_mBAD5262A353BC071C61B8DB462A3D4D5AB5C7C4E (void);
// 0x00000027 System.Void UnityTemplateProjects.SimpleCameraController::OnEnable()
extern void SimpleCameraController_OnEnable_m2BA1F31BEDE84695933E86CF731059D6FAC2111C (void);
// 0x00000028 UnityEngine.Vector3 UnityTemplateProjects.SimpleCameraController::GetInputTranslationDirection()
extern void SimpleCameraController_GetInputTranslationDirection_m924AB4CEA66ADC4F1C63EAC21B660E0495765F4A (void);
// 0x00000029 System.Void UnityTemplateProjects.SimpleCameraController::Update()
extern void SimpleCameraController_Update_m0E587F33074BE72C54BA835B2217B0E8EC94F24A (void);
// 0x0000002A System.Single UnityTemplateProjects.SimpleCameraController::GetBoostFactor()
extern void SimpleCameraController_GetBoostFactor_m42B667ADEACC6F9CF0E4B6549A501609A15716AE (void);
// 0x0000002B UnityEngine.Vector2 UnityTemplateProjects.SimpleCameraController::GetInputLookRotation()
extern void SimpleCameraController_GetInputLookRotation_m4F945850C9036FAF5C3436AA42A1C3CB918E1BB9 (void);
// 0x0000002C System.Boolean UnityTemplateProjects.SimpleCameraController::IsBoostPressed()
extern void SimpleCameraController_IsBoostPressed_m26120B18155EBB68597EA52F392AD1E59F9A71E0 (void);
// 0x0000002D System.Boolean UnityTemplateProjects.SimpleCameraController::IsEscapePressed()
extern void SimpleCameraController_IsEscapePressed_mC694179281D2F4B20E831995751C70A26806488D (void);
// 0x0000002E System.Boolean UnityTemplateProjects.SimpleCameraController::IsCameraRotationAllowed()
extern void SimpleCameraController_IsCameraRotationAllowed_mABB9E26C1CC5C5643B305685F046B4B8493C7AFA (void);
// 0x0000002F System.Boolean UnityTemplateProjects.SimpleCameraController::IsRightMouseButtonDown()
extern void SimpleCameraController_IsRightMouseButtonDown_m0DE4F75C0A0AE963F333712F226000E0AE2DC32A (void);
// 0x00000030 System.Boolean UnityTemplateProjects.SimpleCameraController::IsRightMouseButtonUp()
extern void SimpleCameraController_IsRightMouseButtonUp_mEDE93C76DE7120043B15F390865340D1C5165CE5 (void);
// 0x00000031 System.Void UnityTemplateProjects.SimpleCameraController::.ctor()
extern void SimpleCameraController__ctor_mC6AE2509DDF461856450EC7DE0058A1687AB5C87 (void);
// 0x00000032 System.Void UnityTemplateProjects.SimpleCameraController/CameraState::SetFromTransform(UnityEngine.Transform)
extern void CameraState_SetFromTransform_mAF13C515CFB1085295C01A870D93375E98F16647 (void);
// 0x00000033 System.Void UnityTemplateProjects.SimpleCameraController/CameraState::Translate(UnityEngine.Vector3)
extern void CameraState_Translate_mB8F7239BD9DB70190E59D47D75DD125AD9AF3A96 (void);
// 0x00000034 System.Void UnityTemplateProjects.SimpleCameraController/CameraState::LerpTowards(UnityTemplateProjects.SimpleCameraController/CameraState,System.Single,System.Single)
extern void CameraState_LerpTowards_mF2D4B962A677B281ED2F539A2FFF8A693FB9A326 (void);
// 0x00000035 System.Void UnityTemplateProjects.SimpleCameraController/CameraState::UpdateTransform(UnityEngine.Transform)
extern void CameraState_UpdateTransform_mE653356FD34828D19ECB6793439A14C38F372410 (void);
// 0x00000036 System.Void UnityTemplateProjects.SimpleCameraController/CameraState::.ctor()
extern void CameraState__ctor_m9C5338CABE70B8C73F8A4A08C1AFA1B33417DE9D (void);
static Il2CppMethodPointer s_methodPointers[54] = 
{
	SimpleShoot_Start_m54F4A55B09D39783AA13EC8C78A69811D3ACC861,
	SimpleShoot_Update_m9C86B96646484025B6FAA2BF986ED0370E4299BA,
	SimpleShoot_Shoot_m442002EA156056F2E9CA22132606BC9B8408A6CE,
	SimpleShoot_CasingRelease_m220FA905533E5D3C7628C0FF5E3C2828FDBD4A55,
	SimpleShoot__ctor_mB5F461A08C2C7E7956B0AADEC89F252972313AA8,
	AI_Start_m84B3EA629AEA323FDE537C483BE498BA1E047D61,
	AI_Update_m44C17A8324CC9BF2827D98A82A1B26A8236E1CD4,
	AI_EnemyPath_mAB1712BD9A2CEFBF342933A95E28C136036CDFC9,
	AI_FollowPlayer_m5DA2804644A20D44E2A589BAAFB8E0E1354F903E,
	AI__ctor_m8D5C288A0A62DF5D6327580B33CF073B77D18026,
	CameraLook_Start_m805BDFF07875BAB3146A92FABD13FD6AA1F732CD,
	CameraLook_Update_m8F7AE402EBD962C01D9CC6A3392316380BB70405,
	CameraLook__ctor_mB43C0BBBA42F69696CD1B5AB9DC3F838FF4471AD,
	Menu_Start_m50A1B94A03D9E9BE1E2B69E931A1E02943BB2F36,
	Menu_Update_mDF23A59FAEDA72A8B115C4C3CF2ACCAFC6B415AD,
	Menu_EscenaJuego_m05F7043C4A9ADA7D61DDC0C5D20490C70457406A,
	Menu_Salir_m25A4A2994265CFD84F621FD013BBDDAC2A51E2BA,
	Menu__ctor_m7EE9043A2E9DD126B6F69DD8AE3BC9CBDF6E2134,
	PlayerInteractions_OnTriggerEnter_m4F14223B353C818AF0F0C563C39BDBA62D867118,
	PlayerInteractions__ctor_m156B0757CF638271E3CF68D59F33B17E51802C6A,
	PlayerMovement_Update_mC3491BD6CDFF1FA543B16969144C939B2298052F,
	PlayerMovement__ctor_mBF9F632DD9929DD6FF092A968649A4406BFE397F,
	Bullet_OnCollisionEnter_m828563825F2051114D4AEB3F356E3B12C0A9B424,
	Bullet__ctor_mC7D931FE508342F413FBA79525A4819D4114B3EC,
	Gun_Update_m71CB7FCF54EBD12F5A1452B9E5FC978F3636B04B,
	Gun__ctor_mAB78FE575637ED35491FD9880E4B35EB48689718,
	WeaponSway_Start_m069073D7D5D73F7958FD846EC69C59B3460514CA,
	WeaponSway_Update_m1C67B5A19A29DA854753B4C189A9247836E93B7D,
	WeaponSway_Sway_m791BBE224F45B911464D99B824CDD7B74F6B5EA8,
	WeaponSway__ctor_m66F552B7D71D356878C3B25FE008DEA34A684E19,
	AmmoBox__ctor_m51D58B4CCFE518EA670AEF8F2F56189BACB8F5B6,
	GameManager_get_Instance_m758C0D98BBDEF0C2DCA163A5D60B22DBB5B07232,
	GameManager_set_Instance_mAEF75ABE6135BDD61B9A3AED1606C0449787C5C7,
	GameManager_Awake_m22F42B2A82708B10F652CAD8F2E0A4767110FF30,
	GameManager_Update_mC9303BA7C3117BD861F49F8E36151CC52117E6C1,
	GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D,
	Readme__ctor_mF465410C5B2E598F2685E82CFCE1F42186AFF448,
	Section__ctor_mBAD5262A353BC071C61B8DB462A3D4D5AB5C7C4E,
	SimpleCameraController_OnEnable_m2BA1F31BEDE84695933E86CF731059D6FAC2111C,
	SimpleCameraController_GetInputTranslationDirection_m924AB4CEA66ADC4F1C63EAC21B660E0495765F4A,
	SimpleCameraController_Update_m0E587F33074BE72C54BA835B2217B0E8EC94F24A,
	SimpleCameraController_GetBoostFactor_m42B667ADEACC6F9CF0E4B6549A501609A15716AE,
	SimpleCameraController_GetInputLookRotation_m4F945850C9036FAF5C3436AA42A1C3CB918E1BB9,
	SimpleCameraController_IsBoostPressed_m26120B18155EBB68597EA52F392AD1E59F9A71E0,
	SimpleCameraController_IsEscapePressed_mC694179281D2F4B20E831995751C70A26806488D,
	SimpleCameraController_IsCameraRotationAllowed_mABB9E26C1CC5C5643B305685F046B4B8493C7AFA,
	SimpleCameraController_IsRightMouseButtonDown_m0DE4F75C0A0AE963F333712F226000E0AE2DC32A,
	SimpleCameraController_IsRightMouseButtonUp_mEDE93C76DE7120043B15F390865340D1C5165CE5,
	SimpleCameraController__ctor_mC6AE2509DDF461856450EC7DE0058A1687AB5C87,
	CameraState_SetFromTransform_mAF13C515CFB1085295C01A870D93375E98F16647,
	CameraState_Translate_mB8F7239BD9DB70190E59D47D75DD125AD9AF3A96,
	CameraState_LerpTowards_mF2D4B962A677B281ED2F539A2FFF8A693FB9A326,
	CameraState_UpdateTransform_mE653356FD34828D19ECB6793439A14C38F372410,
	CameraState__ctor_m9C5338CABE70B8C73F8A4A08C1AFA1B33417DE9D,
};
static const int32_t s_InvokerIndices[54] = 
{
	1949,
	1949,
	1949,
	1949,
	1949,
	1949,
	1949,
	1949,
	1949,
	1949,
	1949,
	1949,
	1949,
	1949,
	1949,
	1949,
	1949,
	1949,
	1630,
	1949,
	1949,
	1949,
	1630,
	1949,
	1949,
	1949,
	1949,
	1949,
	1949,
	1949,
	1949,
	3224,
	3187,
	1949,
	1949,
	1949,
	1949,
	1949,
	1949,
	1945,
	1949,
	1932,
	1943,
	1925,
	1925,
	1925,
	1925,
	1925,
	1949,
	1630,
	1678,
	641,
	1630,
	1949,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	54,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
